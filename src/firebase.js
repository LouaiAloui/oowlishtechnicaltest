import firebase from 'firebase'

  // initialize firebase
  var config = {
    apiKey: "AIzaSyCxJ66jhY-Za3Ijqm4lh7pUfFPNCqK86K0",
    authDomain: "brezil.firebaseapp.com",
    databaseURL: "https://brezil.firebaseio.com",
    projectId: "brezil",
    storageBucket: "brezil.appspot.com",
    messagingSenderId: "283825558414",
    appId: "1:283825558414:web:9586a7ac7678e55edbcadf"
  };
  // Initialize Firebase
  firebase.initializeApp(config);


  export const database = firebase.database().ref('/brezil');
  export const auth = firebase.auth();
  export const googleProvider = new firebase.auth.GoogleAuthProvider();
