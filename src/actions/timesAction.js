import { TIMES_STATUS, GET_TIMES } from '../actionTypes';
import { database } from '../firebase';

export function getTimes() {
    return dispatch => {
        // as soon as this function fires show loading to true
        dispatch({
            type: TIMES_STATUS,
            payload: true
        });
        database.on(
            'value',
            snapshot => {
                dispatch({
                    type: GET_TIMES,
                    payload: snapshot.val()
                });
                // once notes are received show loading to false
                dispatch({
                    type: TIMES_STATUS,
                    payload: false
                });
                // wait until something changes and try again
            },
            () => {
                dispatch({
                    type: TIMES_STATUS,
                    payload: -1
                });
            }
        );
    };
}


export function saveTime(time) {
    return dispatch => database.push(time);
}

export function deleteTime(id) {
    return dispatch => database.child(id).remove();
}

export function editTime(id, time) {
    return dispatch => database.child(id).update(time);
}
