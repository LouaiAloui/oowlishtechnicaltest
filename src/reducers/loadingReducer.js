import { TIMES_STATUS, USER_STATUS } from '../actionTypes';

export default function(state = {}, action) {
    switch (action.type) {
        case TIMES_STATUS:
            return { ...state, times: action.payload };
        case USER_STATUS:
            return { ...state, user: action.payload };
        default:
            return state;
    }
}
