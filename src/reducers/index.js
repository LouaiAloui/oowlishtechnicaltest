import { combineReducers } from 'redux';
import userReducer from './userReducer';
import loadingReducer from './loadingReducer';
import timeReducer from './timeReducer';

const rootReducer = combineReducers({
    user: userReducer,
    loading: loadingReducer,
    time: timeReducer
});

export default rootReducer;
