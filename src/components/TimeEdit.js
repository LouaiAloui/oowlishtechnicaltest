import React, { Component } from 'react';
import { connect } from 'react-redux';
import { editTime } from '../actions/timesAction';

class TimeEdit extends Component {
    constructor(props) {
        super(props);
        // state
        this.state = {
            arrivingTime: this.props.time.arrivingTime,
            exitTime: this.props.time.exitTime,
            lunchTime: this.props.time.lunchTime
        };
        // bind
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // handle change
    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    // handle submit
    handleSubmit(e) {
        e.preventDefault();
        const time = {
            arrivingTime: this.state.arrivingTime,
            exitTime: this.state.exitTime,
            lunchTime : this.state.lunchTime,
            uid: this.props.uid
        };
        this.props.editTime(this.props.match.params.id, time);
        this.setState({
            arrivingTime: '',
            exitTime: '',
            lunchTime: ''
        });
        this.props.history.push('/');
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <input
                                    onChange={this.handleChange}
                                    value={this.state.arrivingTime}
                                    type="text"
                                    name="arrivingTime"
                                    className="form-control no-border"
                                    placeholder="Arriving Time..."
                                    required
                                />
                            </div>

                            <div className="form-group">
                                <input
                                    onChange={this.handleChange}
                                    value={this.state.exitTime}
                                    type="text"
                                    name="exitTime"
                                    className="form-control no-border"
                                    placeholder="Exit Time..."
                                    required
                                />
                            </div>

                            <div className="form-group">
                                <input
                                    onChange={this.handleChange}
                                    value={this.state.lunchTime}
                                    type="text"
                                    name="lunchTime"
                                    className="form-control no-border"
                                    placeholder="Lunch Time..."
                                    required
                                />
                            </div>

                            <div className="form-group">
                                <button className="btn btn-primary col-sm-12">Update</button>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
       );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        time: state.times[ownProps.match.params.id],
        uid: state.user.uid
    };
}

export default connect(mapStateToProps, { editTime })(TimeEdit);