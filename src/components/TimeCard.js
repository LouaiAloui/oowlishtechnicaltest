import React from 'react';

const TimeCard = ({children}) => (
    <div className="jumbotron vertical-center">
        <div>{children}</div>
    </div>
);

export default TimeCard;
