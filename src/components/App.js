import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { getTimes, saveTime, deleteTime } from '../actions/timesAction';
import TimeCard from './TimeCard';
import { getUser } from '../actions/userAction';
import { Link } from 'react-router-dom';

class App extends Component {
    constructor(props) {
        super(props);
        // state
        this.state = {
            arrivingTime: '',
            exitTime: '',
            lunchTime: ''
        };
        // bind
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.renderTimes = this.renderTimes.bind(this);
    }

    // handle change
    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    // handle submit
    handleSubmit(e) {
        e.preventDefault();
        const time = {
            arrivingTime: this.state.arrivingTime,
            exitTime: this.state.exitTime,
            lunchTime: this.state.lunchTime,
            uid: this.props.user.uid
        };
        this.props.saveTime(time);
        this.setState({
            arrivingTime: '',
            exitTime: '',
            lunchTime: ''
        });
    }

    // render times
    renderTimes() {
        return _.map(this.props.times, (time, key) => {
            return (
                <TimeCard key={key}>
                    <Link to={`/${key}`}>
                        <h2>Arriving Time : {time.arrivingTime}</h2>
                    </Link>
                    <p>Exit Time : {time.exitTime}</p>
                    <p>Lunch Time : {time.lunchTime}</p>
                    {(time.exitTime - time.arrivingTime - time.lunchTime < 8) ?
                        <p>Expected working hours ( 8 hours )</p>
                        : <p>Above working hours ( 8 hours )</p>
                    }
                    {time.uid === this.props.user.uid && (
                        <button className="btn btn-danger btn-xs" onClick={() => this.props.deleteTime(key)}>
                            Delete
                        </button>
                    )}
                </TimeCard>
            );
        });
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-3">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <input
                                    onChange={this.handleChange}
                                    value={this.state.arrivingTime}
                                    type="text"
                                    name="arrivingTime"
                                    className="form-control no-border"
                                    placeholder="Arriving Time..."
                                    required
                                />
                            </div>

                            <div className="form-group">
                                <input
                                    onChange={this.handleChange}
                                    value={this.state.exitTime}
                                    type="text"
                                    name="exitTime"
                                    className="form-control no-border"
                                    placeholder="Exit Time..."
                                    required
                                />
                            </div>

                            <div className="form-group">
                                <input
                                    onChange={this.handleChange}
                                    value={this.state.lunchTime}
                                    type="text"
                                    name="lunchTime"
                                    className="form-control no-border"
                                    placeholder="Lunch Time..."
                                    required
                                />
                            </div>

                            <div className="form-group">
                                <button className="btn btn-primary col-sm-12">See your advancement</button>
                            </div>
                        </form>
                        <br />
                        <br />
                        <br />
                        {this.renderTimes()}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        times: state.time,
        user: state.user
    };
}

export default connect(mapStateToProps, { getTimes, saveTime, deleteTime, getUser })(App);