import React, { Component } from 'react';
import { connect } from 'react-redux';
// with withRouter You can get access to the history object’s properties
import { withRouter } from 'react-router-dom';
import { getUser } from '../actions/userAction';
import { getTimes } from '../actions/timesAction';

class LoadingComponent extends Component {
    componentWillMount() {
        const { userLoading, timesLoading } = this.props;
        // if we havent tried to load the user, load user
        if (userLoading === undefined) {
            this.props.getUser();
        }

        // if we havent tried to get times, load times
        if (timesLoading === undefined) {
            this.props.getTimes();
        }
    }

    componentWillReceiveProps(nextProps) {
        // wait for user to get authenticated and try to load times
        if (nextProps.timesLoading === -1 && nextProps.user !== null) {
            this.props.getTimes();
        }
    }

    render() {
        const { userLoading, timesLoading, children } = this.props;
        /**
         * throughout the lifetime of app user and times loading status will
         * keep toggling between true and false
         * when anything other than that toggling state such as true or false is in the state
         * that means the loading operation is setteled and not active
         * that time, show the enclosing components
         * for everything else and inbetween show Loading
         */
        if ((!userLoading && !timesLoading) || this.props.user === null) {
            return <div>{children}</div>;
        } else {
            return (
                <div>
                    <h2>Loading...</h2>
                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        userLoading: state.loading.user,
        timesLoading: state.loading.times,
        user: state.user
    };
}

export default withRouter(connect(mapStateToProps, { getUser, getTimes })(LoadingComponent));
