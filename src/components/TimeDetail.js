import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import TimeCard from './TimeCard';


class TimeDetail extends Component {
    render() {
        const { time } = this.props
        return (
            <TimeCard>
                <h2>Arriving Time : {time.arrivingTime}</h2>
                <p>Exit Time : {time.exitTime}</p>
                <p>Lunch Time : {time.lunchTime}</p>
                {(time.exitTime - time.arrivingTime - time.lunchTime < 8) ?
                    <p>Expected working hours ( 8 hours )</p>
                    : <p>Above working hours ( 8 hours )</p>
                }
                <Link to="/"> &#171; Back</Link>
            </TimeCard>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        time: state.time[ownProps.match.params.id],
        uid: state.user.uid
    }
}

export default connect(mapStateToProps)(TimeDetail);