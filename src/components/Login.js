import React, { Component } from 'react';
import { connect } from 'react-redux';
import { googleLogin } from '../actions/userAction';

class Login extends Component {
    componentWillMount() {
        if (this.props.user !== null) {
            // console.log(this.props.history);
            this.props.history.push('/');
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user !== null) {
            nextProps.history.push('/');
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row text-center">
                    <div className="col-sm-12 jumbotron" style={{ marginTop: '-20px' }}>
                        <h1>FORTALEZA | {new Date().getFullYear()}</h1>
                        <h2>
                            <i>
                                Login to manage your <b>Arriving, Exit and Lunch</b> times!
                            </i>
                        </h2>
                    </div>

                    <div className="col-sm-12">
                        <button className="btn btn-danger btn-lg" onClick={this.props.googleLogin}>
                            Login with Google
                        </button>
                    </div>
                    
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, onwProps) {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps, { googleLogin })(Login);
