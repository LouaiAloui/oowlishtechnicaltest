export const GET_USER = 'GET_USER';

export const USER_STATUS = 'USER_STATUS';

export const GET_TIMES = 'GET_TIMES';
export const TIMES_STATUS = 'TIMES_STATUS';
